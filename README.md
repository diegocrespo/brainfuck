
## Overview
This is a Brainfuck interpreter written in Crystal. It can be run directly on the command line with a string as input, or in gui mode which allows you to step through the code and view the memory, stack pointer, program counter, and output change in real time.
## Installation

To install add this to your shard.yml
```
name: brainfuck
version: 1.0.0

dependencies:
  my-dependency: 
    gitlab: diegocrespo/brainfuck

```

Then run `shards install`

![Brainfuck example gif](brainfuck_gif.gif "Example of program running")

## Usage

To use the command line tool.
```
Windows 
  brainfuck.exe <options> 


Linux/macOS
  ./brainfuck <options> 

Examples
 ./brainfuck --help
 ./brainfuck -d \"+++++++++[>++++++++++<-]>.\"
 ./brainfuck.exe --gui="test_file.txt"
 Where test_file.txt has brainfuck code like
 >++++++++[<+++++++++>-]<.>++++[<+++++++>-]<+.+++++++..+++.>>++++++[<+++++++>-]<++.------------.>++++++[<+++++++++>-]<+.<.+++.------.--------.>>>++++[<++++++++>-]<.+
```

## Contributing

1. Fork it (<https://github.com/your-github-user/brainfuck/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Diego Crespo](https://github.com/your-github-user) - creator and maintainer
