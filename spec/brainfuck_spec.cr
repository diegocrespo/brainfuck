require "./spec_helper"

describe Interpreter do
  it "interprets '+' and '-'" do
    interpreter = Interpreter.new("+++++-")
    interpreter.interpret
    check_arr = Array(UInt8).new(30_000, 0)
    check_arr[0] = 4
    interpreter.mem.should eq check_arr
  end

  it "Storing the start and end of loops" do
    interpreter = Interpreter.new("+++++++++[>++++++++++<-]>.")
    check_hash = {9 => 23, 23 => 9}
    interpreter.bracket_map.should eq check_hash
  end

  it "Wrapping with +" do
# 256 + 
    interpreter = Interpreter.new("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    interpreter.interpret
    interpreter.@mem[interpreter.@sp].should eq 0
  end
  it "Wrapping with -" do
    interpreter = Interpreter.new("-")
    interpreter.interpret
    interpreter.@mem[interpreter.@sp].should eq 255
  end      
end
