require "option_parser"
require "colorize"
require "./interpreter"
require "./gui"

# the command line module for the Brainfuck `Interpreter`
# ## Examples
# ```
# Windows
# brainfuck.exe
# Linux/macOS
# ./brainfuck
# Examples
# ./brainfuck --help
# ./brainfuck -d \"+++++++++[>++++++++++<-]>.\"
# ```
module Brainfuck
  VERSION = "1.0.0"
  GUI     = Nil
  OptionParser.parse do |parser|
    parser.banner = "BrainFuck Interpreter version: #{VERSION}"
    parser.on "-v", "--version", "Show VERSION" do
      puts "VERSION: #{VERSION}"
      exit
    end
    parser.on "-h", "--help", "Show help" do
      puts parser
      exit
    end
    parser.on "-e", "--example", "Show common examples" do
      example_string = "
Windows

brainfuck.exe

Linux/macOS

./brainfuck

Examples

./brainfuck --help
./brainfuck -d \"+++++++++[>++++++++++<-]>.\"
"
      puts "#{example_string}"
      exit
    end
    parser.on "-g file", "--gui=file", "Run in graphical mode" do |file|
      if File.exists?(file)
        new_file = File.new(file)
        # remove \n and then replace additional \n
        content = new_file.gets_to_end.chomp.gsub("\n","")
        run(content)
        new_file.close
      else
        raise BrainFuckError.new("#{file} does not exist")
      end
    end
    parser.on "-d str", "--data=str", "Run program, pass BF string you'd like to interpret" do |str|
      Interpreter.new(str).interpret
      exit
    end
    parser.missing_option do |option_flag|
      if option_flag == "-d" || option_flag == "--data"
        STDERR.puts "Error: #{option_flag} is missing the BrainFuck String to interpret."
      end

      if option_flag == "-d" || option_flag == "--data"
        STDERR.puts "Error: #{option_flag} is missing the BrainFuck String to interpret."
      else
        STDERR.puts "Error: #{option_flag} is a required value"
      end
      STDERR.puts ""
      STDERR.puts parser
      exit(1)
    end
    parser.invalid_option do |option_flag|
      STDERR.puts "#{"ERROR:".colorize(:red)} #{option_flag} is not a valid option."
      STDERR.puts parser
      exit(1)
    end
  end
end
