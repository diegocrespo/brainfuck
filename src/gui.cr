require "raylib-cr"
require "./interpreter"
alias Rl = Raylib
alias Key = Rl::KeyboardKey
#Runs the GUI that powers the brainfuck interpreter 
def run(data)
  bf = Interpreter.new(data)
  padding = 5
  screen_width = 1650
  screen_height = 450
  running = false
  text_output = "Output: "
  # controls the output and keeps it from printing if
  # the user doesn't continue to step
  stepped = false
  end_of_program = false

  Rl.init_window(screen_width, screen_height, "Hello World")
  Rl.set_target_fps(120)
  until Rl.close_window?
    Rl.begin_drawing
    Rl.clear_background(Rl::RAYWHITE)
    # set program to running and stop showing instructions
    if Rl.key_released?(Key::Space)
      running = true
    end

    if running
      if bf.@pc == bf.@length - 1
        text_width = Rl.measure_text("END OF CODE", 40)
        Rl.draw_text("END OF CODE", screen_width/2 - (text_width / 2), screen_height/2 +60, 40, Rl::RED)
        end_of_program = true

      end
      Rl.draw_text("Current Symbol: #{bf.code[bf.pc]}", 0 + padding, 0, 20, Rl::BLACK)
      Rl.draw_text("Program Counter: #{bf.pc}", 0 + padding, 20, 20, Rl::BLACK)
      Rl.draw_text("Stack Pointer: #{bf.sp}", 0 + padding, 40, 20, Rl::BLACK)
      Rl.draw_text("Memory: #{bf.mem[0..10]}", 0 + padding, 60, 20, Rl::BLACK)
      # code for highlighting the character in the brainfuck code you are on
      font_size = 20.0
      code_start =( bf.@pc -1 >= 0) ? bf.@pc -1 : bf.@pc
      code_end = (bf.@pc +14 < bf.@length) ? bf.@pc +14 : bf.@length
      substring = bf.@code[code_start..code_end]
       

      substring.each_char.with_index do |char, index|
        if index == 0
          Rl.draw_text_codepoint(Rl.get_font_default,
            char.ord,
            Rl::Vector2.new(x: padding + (index * font_size), y: 110),
            font_size, Rl::RED)
        else
          Rl.draw_text_codepoint(Rl.get_font_default,
            char.ord,
            Rl::Vector2.new(x: padding + (index * font_size), y: 110),
            font_size, Rl::BLACK)
        end
      end
      # consumes a single brainfuck instruction
      if (Rl.key_released?(Key::Right) || Rl.key_down?(Key::Up)) && !end_of_program
        bf.consume
        stepped = true
        if bf.@code[bf.@pc] == '.' && stepped
          text_output = text_output + bf.@mem[bf.@sp].unsafe_chr
          stepped = false
        end        
      elsif (Rl.key_pressed?(Key::Enter))
        while (bf.@pc != bf.@length -1)
          bf.consume
          stepped = true
          if bf.@code[bf.@pc] == '.' && stepped
            text_output = text_output + bf.@mem[bf.@sp].unsafe_chr
            stepped = false
          end          
        end
      end
      # append the character to the output message
      if bf.@code[bf.@pc] == '.' && stepped
        text_output = text_output + bf.@mem[bf.@sp].unsafe_chr
        stepped = false
      end
      if text_output 
        Rl.draw_text(text_output, 0 + padding, 80, 20, Rl::BLACK)
        stepped = false
      end

    else
      # if the program isn't running display instructions
      init_message = "Press Space to Start"
      init_size = 40
      text_width = Rl.measure_text(init_message, init_size)
      Rl.draw_text("Step through the code with the Right Arrow", padding, 0, 20, Rl::BLACK)
      Rl.draw_text("Hold Up Arrow to quickly iterate through the code", padding, 20, 20, Rl::BLACK)
      Rl.draw_text("Hit Enter to run code through until the end", padding, 40, 20, Rl::BLACK)      
      Rl.draw_text(init_message, (screen_width / 2) - (text_width / 2), screen_height / 2, init_size, Rl::BLACK)
    end
    Rl.end_drawing
  end
end
