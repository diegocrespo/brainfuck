# Overrides expection to call a custom
# BrainFuckError. Currently used just
# to handle the case where an improper
# command passed in the `-d` or `--data` flag
class BrainFuckError < Exception
  def initialize(msg)
    super("BrainFuck Error: #{msg}")
  end
end

# This is the library for handling brainfuck code.
# The struct `Interpreter` can be instantiated like so...
# ```
# Interpreter.new("+++++++++[>++++++++++<-]>.")
# ```
# Where the arguments to the initializer is a string containing the brainfuck code.
struct Interpreter
  # A 30k length array of memory as specified in the BF spec. It starts initialize to 0
  getter mem = Array(UInt8).new(30_000, 0)
  # The **P**rogram **C**ounter keeps track of how far along we are in the code
  # And is set to the start or end of the loop when it hits `[` or `]`
  getter pc = 0
  # **S**tack **P**ointer indexes into the area in memory that is modified by the
  # `+` and `-` instructions
  getter sp = 0
  # The code string that is passed to the program by the user with `-d` and `--data` flags
  getter code : String
  # Stores the start and end positions of loops in a hash
  getter bracket_map = Hash(Int32, Int32).new
  # Iterates through the entire code string and saves the start
  # and end positions of the loop brackets in `bracket_map`.
  # Is called during initialization

  @length = 0

  # consumes a single char at a time
  def consume
    if @pc < @length
      char = @code[@pc]
      case char
      when '+'
        if @mem[@sp] == 255
          @mem[@sp] = 0
        else
          @mem[@sp] += 1
        end
      when '-'
        if @mem[@sp] == 0
          @mem[@sp] = 255
        else
          @mem[@sp] -= 1
        end
      when '.'
        puts @mem[@sp].chr
      when '>'
        @sp += 1 if (@sp + 1) < @mem.size
      when '<'
        @sp -= 1 if (@sp - 1) >= 0
      when '['
        @pc = @bracket_map[@pc] if @mem[@sp] == 0
        # Hops back and forth between start and end brackets
        # Until the loop has counted down to zero
      when ']'
        @pc = @bracket_map[@pc] if @mem[@sp] != 0
        # !!!TODO this will have to be modified in gui mode
      when ','
        loop do
          print "Enter a number between 0 and 255: "
          input = gets
          # check for Ctrl + d/c
          if input.nil?
            exit
          else
            # check that user entered a number
            if input.chomp =~ /^(25[0-5]|2[0-4]\d|1\d{2}|[1-9]\d|\d)$/
              num = input.chomp.to_u8
              @mem[@sp] = num
              break
            else
              puts "Please Enter a number between 0 and 255. #{input.chomp} is not acceptable"
            end
          end
        end
      else
        raise BrainFuckError.new("#{char} is not a valid BrainFuck command")
      end
      @pc += 1
    end
  end

  def create_bracket_map(str : String)
    start : Int32 | Nil = nil
    str.each_char_with_index do |char, index|
      if char == '['
        start = index
      elsif char == ']'
        if start
          @bracket_map[start] = index
          @bracket_map[index] = start
        end
      end
    end
  end

  def initialize(str : String)
    @code = str
    create_bracket_map(@code)
    @length = @code.size
  end

  # Iterates through the code string and compares each character
  # against a case statement for that character
  # raises a custom `BrainFuckError` when a unknown BF command is given
  def interpret
    while @pc < @length
      char = @code[@pc]
      case char
      when '+'
        if @mem[@sp] == 255
          @mem[@sp] = 0
        else
          @mem[@sp] += 1
        end
      when '-'
        if @mem[@sp] == 0
          @mem[@sp] = 255
        else
          @mem[@sp] -= 1
        end
      when '.'
        puts @mem[@sp].chr
      when '>'
        @sp += 1 if (@sp + 1) < @mem.size
      when '<'
        @sp -= 1 if (@sp - 1) >= 0
      when '['
        @pc = @bracket_map[@pc] if @mem[@sp] == 0
        # Hops back and forth between start and end brackets
        # Until the loop has counted down to zero
      when ']'
        @pc = @bracket_map[@pc] if @mem[@sp] != 0
      when ','
        loop do
          print "Enter a number between 0 and 255: "
          input = gets
          # check for Ctrl + d/c
          if input.nil?
            exit
          else
            # check that user entered a number
            if input.chomp =~ /^(25[0-5]|2[0-4]\d|1\d{2}|[1-9]\d|\d)$/
              num = input.chomp.to_u8
              @mem[@sp] = num
              break
            else
              puts "Please Enter a number between 0 and 255. #{input.chomp} is not acceptable"
            end
          end
        end
      else
        raise BrainFuckError.new("#{char} is not a valid BrainFuck command")
      end
      @pc += 1
    end
  end
end
